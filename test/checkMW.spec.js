const assert = require('assert');
const sinon = require('sinon');
const settings = require('../src/settings');
const checks = require('../src/checks');
const setHealth = require('../src/setHealth');
const {
  DOWN,
  UP
} = require('../src/stateTypes');
const {
  MONGO_DB
} = require('../src/checkTypes');

const sb = sinon.createSandbox();

sb.stub(settings, 'dependsOn').callsFake(() => true);

sinon.spy(checks, 'MONGO_DB');

const checkMW = require('../src/checkMW');

describe('The HealthCheck Middleware', () => {
  it('should identify checkable dependencies', async () => {
    await checkMW(undefined, { status: () => ({ json: sinon.stub() }) });
    assert.equal(checks.MONGO_DB.callCount, 1);
  });

  it('should set and check status', async () => {
    const json = sinon.stub();
    const res = { status: () => ({ json }) };

    setHealth(MONGO_DB, UP, 'MongoDb derreteu!');

    await checkMW(undefined, res);

    const result = json.firstCall.args[0];
    const { integrations } = result.data;

    assert.equal(integrations.MONGO_DB.error, 'MongoDb derreteu!');

    assert.equal(integrations.MONGO_DB.serviceStatus, UP);
  });
});
