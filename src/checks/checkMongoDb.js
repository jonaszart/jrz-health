const { MONGO_DB } = require('../checkTypes');

module.exports = () => {
  const serviceStatus = process.env[`HEALTH_CHECK_${MONGO_DB}`];
  const error = process.env[`HEALTH_CHECK_${MONGO_DB}_ERROR`];

  return { serviceStatus, error, name: MONGO_DB };
};
