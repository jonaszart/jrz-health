const checkMongoDb = require('./checkMongoDb');
const {
  MONGO_DB
} = require('../checkTypes');

const checks = {
  setCheck: (checkType, check) => { checks[checkType] = check; }
};

checks[MONGO_DB] = checkMongoDb;

module.exports = checks;
