const settings = require('./settings');
const { DOWN, UP } = require('./stateTypes');
const checks = require('./checks');
const types = require('./checkTypes');

const dependencyChecks = [];
if (settings.dependsOn('jrz-mongodb')) dependencyChecks.push(checks[types.MONGO_DB]);

const version = settings.getVersion();

const statusReducer = (prev, curr) => (curr.serviceStatus === DOWN ? DOWN : prev);
const integrationsReducer = (hash, item) => {
  // eslint-disable-next-line no-param-reassign
  hash[item.name] = item;
  return hash;
};

const checkMW = async (req, res) => {
  const results = await Promise.all(dependencyChecks.map(check => check()));
  const status = results.reduce(statusReducer, UP);
  const integrations = results.reduce(integrationsReducer, {});
  const success = status === UP;
  const data = {
    dateTime: new Date(), integrations, status, version
  };
  const error = results
    .filter(check => check.serviceStatus === DOWN)
    .map(check => `${check.name}: ${check.error}`)
    .join(' - ');

  res.status(200).json({ success, data, error });
};

module.exports = checkMW;
