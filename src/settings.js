// eslint-disable-next-line import/no-dynamic-require
const { dependencies, version } = require(`${process.cwd()}/package.json`);

const dependsOn = module => (dependencies[module] !== undefined);
const getVersion = () => version;

module.exports = { dependsOn, getVersion };
