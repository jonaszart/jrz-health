const setHealth = (check, state, description) => {
  process.env[`HEALTH_CHECK_${check}`] = state;
  process.env[`HEALTH_CHECK_${check}_ERROR`] = description;
};

module.exports = setHealth;
