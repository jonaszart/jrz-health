const DOWN = 'DOWN';
const UP = 'UP';

module.exports = {
  DOWN,
  UP
};
