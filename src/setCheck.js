const checks = require('./checks');

module.exports = (checkType, check) => {
  checks.setCheck(checkType, check);
};
