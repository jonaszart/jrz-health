const checkMW = require('./src/checkMW');
const setHealth = require('./src/setHealth');
const setCheck = require('./src/setCheck');
const stateTypes = require('./src/stateTypes');
const checkTypes = require('./src/checkTypes');

const setHealthDown = (checkType, desc) => setHealth(checkType, stateTypes.DOWN, desc);
const setHealthUp = (checkType, desc) => setHealth(checkType, stateTypes.UP, desc);

module.exports = {
  checkMW,
  setHealth,
  setCheck,
  setHealthDown,
  setHealthUp,
  ...stateTypes,
  ...checkTypes
};
